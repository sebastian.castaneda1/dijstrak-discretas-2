//elaborado por Sebastian Castañeda Quirama cc 1000549513
//            y Samuel David Montoya Cano 1001137913

import java.util.Scanner;

public class Dijsktra {
    private static void dijskstra(int[][] matrizPeso, int llegada) {
        int v = matrizPeso.length;
        boolean visitado[] = new boolean[v];
        int distancia[] = new int[v];
        int puntoFinal = llegada;
        double resultado;

        for (int i = 1; i < v; i++) {
            distancia[i] = Integer.MAX_VALUE;
        }
        for (int i = 0; i < v; i++) {
            //encontrar vertice con menor distacia
            int minVertice = vMenor(distancia, visitado);
            visitado[minVertice] = true;
            //revisar vecinos
            for (int j = 0; j < v; j++) {
                if (matrizPeso[minVertice][j] != 0 && !visitado[j] && distancia[minVertice] != Integer.MAX_VALUE) {
                    int nuevaDistancia = distancia[minVertice] + matrizPeso[minVertice][j];
                    if (nuevaDistancia < distancia[j]) {
                        distancia[j] = nuevaDistancia;
                    }
                }
            }
        }
        //imprimir
        for (int i = 0; i < v; i++) {
            if (i == puntoFinal) {
                resultado = distancia[i];
                System.out.println("La forma mas eficaz de legar al nodo " + i +" da un total de " +resultado/10);
            }
        }
    }

    private static int vMenor(int[] distance, boolean visited[]) {
        int minVertice = -1;
        for (int i = 0; i < distance.length; i++) {
            if (!visited[i] && (minVertice == -1 || distance[i] < distance[minVertice])) {
                minVertice = i;
            }
        }
        return minVertice;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese la matriz de peso y presione enter: ");
        int v = s.nextInt();
        int e = s.nextInt();
        int matrixPeso[][] = new int[v][v];
        int llegada;
        for (int i = 0; i < e; i++) {
            int v1 = s.nextInt();
            int v2 = s.nextInt();
            int peso = s.nextInt();
            matrixPeso[v1][v2] = peso;
            matrixPeso[v2][v1] = peso;
        }
        System.out.println("Ingrese el vertice al que desea llegar");
        llegada = s.nextInt();
        dijskstra(matrixPeso, llegada);
    }
}
/*matriz de pesos
       Siempre analizara la minima distancia entre el nodo A (0) y el nodo escogido(llegada)
       En el primer valor se encuentra el nodo 0 y en el segundo el nodo 1, siendo a y b respectivamente,
       luego el 60 representa el 6 el cual es el valor de peso que hay entre a y b,
       estando este multiplicado por 10 para que sea un entero los numeros decimales, y asi para cada linea de la matriz

       (la cantidad de nodos y aristas del sistema es la primera linea de la matriz)  copiar y pegar matriz para ejecutar el programa
        28	48
        0	1	60
        0	2	14
        0	3	25
        1	2	20
        1	4	80
        2	5	99
        3	5	82
        3	6	200
        4	5	70
        4	10	160
        5	8	140
        5	9	169
        5	10	180
        6	7	140
        6	8	180
        7	8	160
        7	18	85
        8	9	90
        8	17	100
        9	10	160
        9	15	65
        9	17	80
        10	11	200
        10	12	40
        11	12	80
        11	13	40
        12	15	50
        13	14	10
        13	27	100
        14	15	43
        14	24	35
        15	16	90
        16	17	110
        16	19	120
        16	24	20
        17	18	120
        18	19	43
        19	20	70
        20	21	17
        20	24	152
        21	22	15
        21	23	153
        22	26	30
        23	25	28
        23	26	50
        24	25	10
        25	27	20
        26	27	40

 */
